<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Src\\Payment\\CashOnDelivery' => $baseDir . '/Src/Payment/CashOnDelivery.php',
    'Src\\Payment\\Factory' => $baseDir . '/Src/Payment/Factory.php',
    'Src\\Payment\\Pay' => $baseDir . '/Src/Payment/Pay.php',
    'Src\\Payment\\PaymentMethod' => $baseDir . '/Src/Payment/PaymentMethod.php',
    'Src\\Payment\\Stripe' => $baseDir . '/Src/Payment/Stripe.php',
    'Src\\Shop\\Clothing' => $baseDir . '/Src/Shop/Clothing.php',
    'Src\\Shop\\Electronics' => $baseDir . '/Src/Shop/Electronics.php',
    'Src\\Shop\\Factory' => $baseDir . '/Src/Shop/Factory.php',
    'Src\\Shop\\Order' => $baseDir . '/Src/Shop/Order.php',
    'Src\\Shop\\Product' => $baseDir . '/Src/Shop/Product.php',
);
