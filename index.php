<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 12:49 PM
 */
ini_set('display_errors',1);
error_reporting(E_ALL);
include 'vendor/autoload.php';
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $response = [
        'items' =>[
            [
                'name' => 'Monitor',
                'value' => 'Electronics',
                'price' => 100
            ],
            [
                'name' => 'T-shirt',
                'value' => 'Clothings',
                'price' => 50
            ]
        ],
        'payment_method' =>[
            'STRIPE',
            'CASH_ON_DELIVERY'
        ]

    ];
     die(json_encode($response));
}
$product = $_POST['product'];
$quanity = $_POST['quantity'];
$method = $_POST['method'];
$productObj = \Src\Shop\Factory::create($product);
$methodObj = \Src\Payment\Factory::create($method);
$order = new Src\Shop\Order($quanity);
$order->setProduct($productObj);
$order->setPaymentMethod($methodObj);

$pay = new \Src\Payment\Pay($order);
$message = $pay->pay();

$response = $pay->result().$message;
die(json_encode($response));