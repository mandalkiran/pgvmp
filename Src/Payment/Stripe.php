<?php

/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 1:16 PM
 */
namespace Src\Payment;

class Stripe implements PaymentMethod
{
    private $ccInfo;

    /**
     * Stripe constructor.
     * @param $ccInfo
     */
    public function __construct($ccInfo)
    {
        $this->ccInfo = $ccInfo;
    }

    function getPayMethod()
    {
        return 'Stripe';
        // TODO: Implement getPayMethod() method.
    }

    function isOnlineTransaction()
    {
        return true;
    }
    
    function makeTransactions($total){
        return $this->ccInfo == '1111' ? 'Successfully deducted '.$total.' ': 'wrong cc info! enter 1111' ;
    }


}