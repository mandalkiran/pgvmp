<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 1:22 PM
 */
namespace Src\Payment;

interface PaymentMethod {
    
    function getPayMethod();
    function isOnlineTransaction();
    
}