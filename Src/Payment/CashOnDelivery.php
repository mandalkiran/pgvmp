<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 2:17 PM
 */

namespace Src\Payment;


class CashOnDelivery implements PaymentMethod
{
    function getPayMethod()
    {
        return 'Cash On Delivery';
    }

    function isOnlineTransaction()
    {
        return false;
    }
}