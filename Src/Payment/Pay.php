<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 1:49 PM
 */

namespace Src\Payment;


use Src\Shop\Product;

class Pay
{
    private $product;
    private $paymentMethod;
    private $order;

    /**
     * Pay constructor.
     */
    public function __construct($order)
    {
        $this->order = $order;
    }
    
    
    public function pay(){
       
        return $this->order->paymentMethod->isOnlineTransaction() ? $this->order->paymentMethod->makeTransactions( $this->order->getTotal() ) : true;
    }

    public function result(){
        return $this->order->product->getName().' has been successfully purchased with Payment Option '. $this->order->paymentMethod->getPayMethod().'. ';
    }

    
}