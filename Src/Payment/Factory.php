<?php

/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 1:15 PM
 */
namespace Src\Payment;

class Factory
{
    public static function create($product){

        switch ($product){
            case 'CASH_ON_DELIVERY':
                return new CashOnDelivery();
            case 'STRIPE':
                $ccInfo = $_POST['cc_info'];
                return new Stripe($ccInfo);
            default:
                return [];
        }

    }
}