<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 12:55 PM
 */

namespace Src\Shop;


class Electronics implements Product
{
    public function getName(){
        return 'Monitor';
    }
    
    public function getPrice(){
        return 100;
    }

}