<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 1:00 PM
 */

namespace Src\Shop;


interface Product{
    
    
    public function getName();

    public function getPrice();
    

}