<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 12:52 PM
 */

namespace Src\Shop;


class Factory{
    
    public static function create($product){
        
        switch ($product){
            case 'Electronics':
                return new Electronics();
            case 'Clothings':
                return new Clothing();
        }

        return [
            'error'
        ];
    }
    
    
}