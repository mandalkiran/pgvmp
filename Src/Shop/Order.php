<?php
/**
 * Created by PhpStorm.
 * User: kmandal
 * Date: 2/17/19
 * Time: 3:46 PM
 */

namespace Src\Shop;


use Src\Payment\PaymentMethod;

class Order
{
    private $quantity = 1;
    public $product;
    public $paymentMethod;

    /**
     * Order constructor.
     * @param $quantity
     */
    public function __construct($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setProduct(Product $product){
        $this->product = $product;
    }
    
    public function setPaymentMethod(PaymentMethod $paymentMethod){
        $this->paymentMethod = $paymentMethod;
    }

    public function getTotal(){
       return $this->product->getPrice() * $this->quantity;
        
    }

}